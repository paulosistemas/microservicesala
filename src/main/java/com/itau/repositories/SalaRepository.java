package com.itau.repositories;

import com.itau.models.Sala;
import org.springframework.data.repository.CrudRepository;

public interface SalaRepository extends CrudRepository<Sala, Integer> {

}
