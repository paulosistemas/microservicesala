package com.itau.controllers;

import com.itau.models.Sala;
import com.itau.services.SalaService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/sala")
public class SalaController {

    @Autowired
    private SalaService salaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Sala registrarSala(@RequestBody @Valid Sala sala){
        return salaService.salvarSala(sala);
    }

    @GetMapping("/{idSala}")
    @ResponseStatus(HttpStatus.OK)
    public Sala buscarSalaPorId(@PathVariable(name="idSala") int idSala){
        try{
            return salaService.buscarSalaPorId(idSala);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
