package com.itau.services;

import com.itau.models.Sala;
import com.itau.repositories.SalaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SalaService {

    @Autowired
    SalaRepository salaRepository;

    public Sala salvarSala(Sala sala){
        return salaRepository.save(sala);
    }

    public Sala buscarSalaPorId(int idSala){
        Optional<Sala> optionalSala = salaRepository.findById(idSala);

        if (optionalSala.isPresent()){
            return optionalSala.get();
        }

        throw new RuntimeException("Sala não encontrada, ID informado: " + idSala);
    }

}
